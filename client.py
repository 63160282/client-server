import socket

# Create a socket object
client_socket = socket.socket()

# Define the IP address and port
ip = "172.20.10.11"
port = 12345

# Connect to server
client_socket.connect((ip, port))

# Read from the socket and write into buffer until EOF
buffer = ""
while True:
    data_received = client_socket.recv(1024).decode()
    if not data_received:
        break
    buffer += data_received

# Print the contents
print(buffer)

# Close
client_socket.close()
