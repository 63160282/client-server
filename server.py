import socket

# Create a socket object
server_socket = socket.socket()
print("Socket successfully created")

# Define the IP address and port
ip = "172.20.10.11"
port = 12345

# Bind the socket to the IP and port
server_socket.bind((ip, port))
print("Socket binded to %s:%s" % (ip, port))

server_socket.listen(5)
print("Socket is listening")

file_path = r"D:\client-server\a.txt"
with open(file_path, "r") as file:
    file_contents = file.read()
    while True:
        client_socket, addr = server_socket.accept()
        print('Got connection from', addr)

        # Send file to the client
        client_socket.send(file_contents.encode())

        # Close client
        client_socket.close()

